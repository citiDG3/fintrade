package com.citi.hackathon.HackathonSpringBootRESTAPI;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import com.citi.hackathon.HackathonSpringBootRESTAPI.service.TradeServiceImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ServiceTest {
    @Autowired
    
    TradeServiceImpl service;
    

    @Test
    public void findAllreturnsTrades() {
        List<Trade> trades = (List<Trade>) service.getTrade();
        int actual = trades.size();
        assertTrue(actual >= 0);
    }

    @Test
    public void checkCountOfTradesAfterInsertion() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 28);
        Date dt = calendar.getTime();
        List<Trade> trades = (List<Trade>) service.getTrade();
        int actual = trades.size();
        Trade trade = new Trade("5f632ee1f044c4455c0c98df", dt, "AAPL", 1.33, 7.82, "SELL", "CREATED");

        service.addTrade(trade);

        List<Trade> tradesAfter = (List<Trade>) service.getTrade();
        int afterInsert = tradesAfter.size();
        service.deleteTrade("5f632ee1f044c4455c0c98df");
        assertTrue(afterInsert > actual);
    }

    @Test
    public void checkCountOfTradesAfterDeletion() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 28);
        Date dt = calendar.getTime();

        Trade trade = new Trade("5f632ee1f044c4455c0c49df", dt, "AMZN", 42.34, 6.88, "BUY", "CREATED");
        service.addTrade(trade);
        List<Trade> trades = (List<Trade>) service.getTrade();
        int actual = trades.size();

        service.deleteTrade("5f632ee1f044c4455c0c49df");
        List<Trade> tradesAfter = (List<Trade>) service.getTrade();
        int afterDelete = tradesAfter.size();
        assertTrue(actual > afterDelete);
    }

    @Test
    public void checkTradeTickersAreDifferentAfterUpdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 28);
        Date dt = calendar.getTime();

        Trade trade = new Trade("5f632ee1f044c4455c0c94df", dt, "C", 123.3, 678.8, "SELL", "CREATED");
        service.addTrade(trade);
        String currentTicker = trade.getTicker();

        trade.setTicker("BASIL");
        service.modifyTradeById(trade);
        String newTicker = trade.getTicker();
        service.deleteTrade("5f632ee1f044c4455c0c94df");
        assertNotEquals(currentTicker, newTicker);

    }

   /*
      @Test public void checkIfTradeIsReturnedAccoringToId() {
        Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.YEAR, 2020);
       calendar.set(Calendar.MONTH, 9);
      calendar.set(Calendar.DATE, 28);
       Date dt = calendar.getTime();
      
      List<Trade> trades = (List<Trade>) service.getTrade(); 
      Trade trade = new Trade("5f632ee1f044c4455c0c28df", dt, "Citi", 12.33, 67.88,"BUY", "CREATED");
      
      service.addTrade(trade); int totalID = trades.size(); 
      Optional<Trade> tradesByID = (Optional<Trade>) service.getTradeById("5f632ee1f044c4455c0c28df"); // List<Trade>
      List<Trade> totalTradeByID = tradesByID.get(); 
     // tradesByID.hashCode();
      totalTradeByID.size();
      assertTrue(totalID < tradesByID.hashCode());
      
      }
      */
    
}
