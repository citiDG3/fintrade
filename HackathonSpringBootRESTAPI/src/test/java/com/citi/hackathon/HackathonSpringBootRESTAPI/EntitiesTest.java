package com.citi.hackathon.HackathonSpringBootRESTAPI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import java.util.Calendar;
import java.util.Date;
import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EntitiesTest {

    @Test
    public void checkAllGettersAreWorkingForTrade() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 28);
        Date dt = calendar.getTime();
        Trade trade = new Trade("5f632ee1f044c4455c0c45df", dt, "C", 12.33, 67.88, "SELL", "CREATED");

        assertEquals(trade.getId(),"5f632ee1f044c4455c0c45df");
        assertEquals(trade.getAmount(), 12.33);
        assertEquals(trade.getCreated(), dt);
        assertEquals(trade.getState(), "CREATED");
        assertEquals(trade.getTicker(), "C");
        assertEquals(trade.getRequestedPrice(), 67.88);
        assertEquals(trade.getType(), "SELL");

    }

    @Test
    public void checkAllSettersAreWorkingForTrade() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 28);
        Date dt = calendar.getTime();

        Calendar calendarNew = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2020);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DATE, 21);
        Date dtNew = calendarNew.getTime();

        Trade trade = new Trade("5f632ee1f044c4455c0c65df", dt, "NFLX", 300.31, 161.18, "SELL", "CREATED");
        trade.setId("5f632ee1f044c4455c0c69df");
        trade.setAmount(30.31);
        trade.setCreated(dtNew);
        trade.setState("REJECTED");
        trade.setTicker("TSLA");
        trade.setRequestedPrice(200.12);
        trade.setType("BUY");

        assertNotEquals(trade.getId(),"5f632ee1f044c4455c0c65df");
        assertNotEquals(trade.getAmount(), 300.31);
        assertNotSame(trade.getCreated(), dt);
        assertNotEquals(trade.getState(), "CREATED");
        assertNotEquals(trade.getTicker(), "NFLX");
        assertNotEquals(trade.getRequestedPrice(), 161.18);
        assertNotEquals(trade.getType(), "SELL");

    }

}
