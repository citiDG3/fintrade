package com.citi.hackathon.HackathonSpringBootRESTAPI;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collection;

import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import com.citi.hackathon.HackathonSpringBootRESTAPI.rest.TradeController;
import com.citi.hackathon.HackathonSpringBootRESTAPI.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


@WebMvcTest(TradeController.class)
public class RestTest {

    
	@Autowired
	private MockMvc mockMvc;

	@MockBean
    private TradeService service;
    
    @Test
	public void test_invalidUrlReturnsNotFound() throws Exception {
		this.mockMvc.perform(get("/get/trades")).andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void test_getAllTrades_returnsFromService() throws Exception {
        Trade testTrade = new Trade();
        testTrade.setTicker("TSLA");

        Collection<Trade> trades = Arrays.asList(testTrade);

		when(service.getTrade()).thenReturn(trades);

		this.mockMvc.perform(get("/trade/getAlltrade")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"ticker\":\"TSLA\"")));
    }
    
  

	@Test
	public void test_addTrade_callsService() throws Exception {
        Trade testTrade = new Trade();
        testTrade.setTicker("TSLA");

		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testTrade);

		this.mockMvc.perform(post("/trade").header("Content-Type", "application/json").content(requestJson))
					.andDo(print()).andExpect(status().isOk());

		verify(service, times(1)).addTrade(any(Trade.class));
    }

    

    
   
    
    
}
