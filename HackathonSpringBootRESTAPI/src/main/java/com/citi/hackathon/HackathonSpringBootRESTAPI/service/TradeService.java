package com.citi.hackathon.HackathonSpringBootRESTAPI.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;

import org.springframework.stereotype.Service;

@Service
public interface TradeService {
    void addTrade(Trade t);

    Collection<Trade> getTrade();

    void deleteTrade(String id);

    String modifyTradeById(Trade id);

    Optional<Trade> getTradeById(String id);

}