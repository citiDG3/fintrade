package com.citi.hackathon.HackathonSpringBootRESTAPI.service;

import java.lang.String;
import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.HackathonSpringBootRESTAPI.TradeRepository.TradeRepo;
import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepo repo;

    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public Collection<Trade> getTrade() {

        return repo.findAll();
    }

    @Override
    public void deleteTrade(String id) {
        repo.deleteById(id);

    }

    @Override
    public String modifyTradeById(Trade id) {
        
        repo.save(id);
        return id + " Updated successfully";
    }

    @Override
    public Optional<Trade> getTradeById(String id) {
        return repo.findById(id);
    }

}