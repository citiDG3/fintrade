package com.citi.hackathon.HackathonSpringBootRESTAPI.entities;

import java.util.Date;

//import org.bson.types.StringString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    @Id
    private String id;
    private Date created;
    private String ticker;
    private Double amount;
    private Double requestedPrice;
    private String state;
    private String type;

    public String getId() {
        return id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(Double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Trade(String id, Date created, String ticker, Double amount, Double requestedPrice, String type,
            String state) {
        this.id = id;
        this.created = created;
        this.ticker = ticker;
        this.amount = amount;
        this.requestedPrice = requestedPrice;
        this.state = state;
        this.type = type;
    }

    public Trade() {
    }

}